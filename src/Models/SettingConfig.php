<?php


namespace Finoghentov\NovaSettings\Models;

use Illuminate\Database\Eloquent\Model;

class SettingConfig extends Model
{
    protected $table = 'settings_config';

    protected $guarded = [];

    protected static function seed(){
        self::insert([
            'title' => 'Text Editor',
            'key' => 'config_editor',
            'type' => 'select',
            'value' => 'trumbowyg',
            'options' => '{"ckeditor":"CKeditor","trumbowyg":"Trumbowyg"}'
        ]);

        self::insert([
           'title' => 'Show Hints',
           'key' => 'hint_switcher',
           'type' => 'checkbox',
           'value' => 1
        ]);
    }
}
