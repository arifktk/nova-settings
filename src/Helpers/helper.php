<?php
if(!function_exists('settings')){
    /**
     * Return value for specify setting
     *
     * @param $key string Key Of Value
     * @param $locale null by default
     * @return string
     */
    function setting($key, $locale = null){

        if(!$locale){
            $locale = app()->getLocale();
        }

        if(config('romarkcode-settings.cache_able')){
            if (config('romarkcode-settings.cache_time') == 'forever') {
                return \Illuminate\Support\Facades\Cache::tags(explode('.', $key)[0])
                    ->rememberForever($key.$locale, function () use ($key, $locale) {
                        return \Finoghentov\NovaSettings\Models\Setting::getValue($key, $locale);
                    });
            }

            return \Illuminate\Support\Facades\Cache::tags(explode('.', $key)[0])
                ->remember($key.$locale, config('romarkcode-settings.cache_time'), function () use ($key, $locale) {
                    return \Finoghentov\NovaSettings\Models\Setting::getValue($key, $locale);
                });
        }

        return   \Finoghentov\NovaSettings\Models\Setting::getValue($key, $locale);
    }
}

if(!function_exists('settings_group')){
    /**
     * Return group of values for specify group
     *
     * @param $key string Key Of Value
     * @param $locale null by default
     * @return string
     */
    function settings_group($key, $locale = null){

        if(!$locale){
            $locale = app()->getLocale();
        }

        return \Finoghentov\NovaSettings\Models\Setting::getGroupData($key, $locale);
    }
}

if(!function_exists('setting_helper')){
    /**
     * Return value for specify setting or create new setting
     *
     * @param $group string Key Of Setting group
     * @param $key string Key Of Value
     * @param $type string Type Of setting input
     * @return string
     */
    function setting_helper(string $group, string $key, string $type = 'text_box', $locale = null) {
        $setting_key = $group.'.'.$key;
        $setting = setting($setting_key, $locale);

        if ($setting != $setting_key) {
            return $setting;
        }

        $settingGroup = \Finoghentov\NovaSettings\Models\Setting::firstOrCreate([
            'group_title' => str_replace('_', ' ', ucfirst($group)),
            'group_key' => $group
        ]);
        $setting_data = json_decode($settingGroup->settings_data, true);

        $new_setting_data = [
            $key => [
                "order" => 1,
                "name" => str_replace('_', ' ', ucfirst($key)),
                "type" => $type,
                "value" => [config('app.fallback_locale') => str_replace('_', ' ', ucfirst($key))],
            ]
        ];
        $data = array_merge($setting_data, $new_setting_data);

        $settingGroup->update(['settings_data' => json_encode($data)]);

        return setting($setting_key);
    }
}
