<?php


namespace Finoghentov\NovaSettings\Controllers;


use Finoghentov\NovaSettings\Controllers\ApiController;
use Finoghentov\NovaSettings\Models\SettingConfig;
use Illuminate\Http\Request;

class SettingConfigController extends ApiController
{
    /**
     * Return all settings at json format
     * @return SettingConfig json
     */
    public function getConfig(){
        return parent::api_getConfig();
    }

    /**
     * Upload config
     * @return void
     * @throws \Exception
     */
    public function uploadConfig(){
        return parent::api_uploadConfig();
    }

    /**
     * Saving config
     * @param Request $request
     * @return string
     */
    public function saveConfig(Request $request){
        return parent::api_saveConfig($request);
    }

    /**
     * Getting Config Data
     * @return string
     */
    public function getData(){
        return parent::api_getConfigData();
    }

    /**
     * Getting Editor Value
     * @return string
     */
    public function getEditor(){
        return parent::api_getEditor();
    }
}
