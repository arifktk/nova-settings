<?php



namespace Finoghentov\NovaSettings\Traits;


trait DataSaving
{
    protected function saveSetting($groupId, $key, $input, $request){
        if($input['type'] === 'text_box' || $input['type'] === 'textarea' || $input['type'] === 'rich_text_box'){
            return $this->getTextValues($groupId, $key, $request);
        }

        if($input['type'] === 'checkbox'){
            return $this->getCheckboxValue($groupId, $key, $request);
        }

        if($input['type'] === 'image'){
            return $this->getImageValue($groupId, $key, $input, $request);
        }
    }

    private function getCheckboxValue($groupId, $key, $request){
        $result = 0;

        if(isset($request->field[$groupId][$key])){
            $result = 1;
        }

        return $result;
    }

    private function getTextValues($groupId, $key, $request){
        $input['value'] = [];
        foreach($this->locales as $locale){
            $input['value'][$locale] = $request->field[$groupId][$key][$locale];
        }
        return $input['value'];
    }

    private function getImageValue($groupId, $key, $input, $request){

        if(!is_array($input['value'])){
            $input['value'] = [];
        }

        if(isset($request->field[$groupId][$key])){
            foreach($this->locales as $locale){
                try{
                    $fileName = $this->storeFile($request->field[$groupId][$key][$locale]);
                    $input['value'][$locale] = $fileName;
                }catch (\Exception $e){

                }

            }
        }
        return $input['value'];
    }

    private function storeFile($file){
        $fileName = $file->getClientOriginalName();
        if(file_exists(storage_path('app\public\\'.$fileName))){
            $fileName = $this->uniqueFileName($fileName);
        }
        $file->storeAs('public', $fileName);
        return $fileName;
    }

    private function uniqueFileName($filename){
        $explode = explode('.', $filename);
        $filename = $explode[0].'_'.(time()+rand(0,100000)).'.'.$explode[1];
        return $filename;
    }
}
