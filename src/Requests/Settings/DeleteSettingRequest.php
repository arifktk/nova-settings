<?php

namespace Finoghentov\NovaSettings\Requests\Settings;

use Finoghentov\NovaSettings\Requests\ApiRequest;

class DeleteSettingRequest extends ApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'group_id' => 'required|integer',
            'key' => 'required'
        ];
    }
}
