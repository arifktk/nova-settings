import $ from 'jquery';
window.$ = window.jQuery = $;

Nova.booting((Vue, router, store) => {



    router.addRoutes([
        {
          name: 'nova-settings',
          path: '/nova-settings',
          component: require('./components/Tool'),
        },
        {
            name: 'nova-settings-config',
            path: '/nova-settings-config',
            component: require('./components/Config'),
        },
        {
          name: 'add-group',
          path: '/nova-settings/add-group',
          component: require('./components/NewGroup'),
        },
    ])
})
