<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SettingsConfig extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings_config', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->string('title', 255);
            $table->string('key', 255)->unique();
            $table->string('type', 255);
            $table->string('value', 255);
            $table->string('options', 500)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings_config');
    }
}
