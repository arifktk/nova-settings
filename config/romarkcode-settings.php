<?php

return [

    /**
     * Ability to add settings in cache. By default, its unable
     *
     *  If you want to cache setting puu cache-able= true:
     *  Cache key will be key of setting
     */
    'cache_able' => false,


    /**
     * Set cache time. By default, it's 1 minute.
     * if you want you can set cache time forever just change cache_time = 'forever'
     */
    'cache_time' => 60,
];
